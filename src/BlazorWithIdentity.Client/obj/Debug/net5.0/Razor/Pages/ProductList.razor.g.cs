#pragma checksum "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "eb30ff9203a872d1a40f7f69bf34d0bcf11fedb0"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorWithIdentity.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization.Infrastructure;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using BlazorWithIdentity.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using BlazorWithIdentity.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using BlazorWithIdentity.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using BlazorWithIdentity.Client.Services.Contracts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using BlazorWithIdentity.Client.Services.Implementations;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using BlazorWithIdentity.Client.Services.Implementations.ToastNotification;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using BlazorWithIdentity.Client.States;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using System.Security.Claims;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\_Imports.razor"
using Microsoft.Fast.Components.FluentUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
using BlazorWithIdentity.Shared.DTO.Category;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
using BlazorWithIdentity.Shared.DTO.CompositeProduct;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
using BlazorWithIdentity.Shared.DTO.ProductSku;

#line default
#line hidden
#nullable disable
    public partial class ProductList : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "row row-sm");
            __builder.AddAttribute(2, "b-fajghabgcf");
            __builder.AddMarkupContent(3, "<h6 b-fajghabgcf>Product List</h6>\r\n     <div class=\"col-md-12\" b-fajghabgcf></div>\r\n     ");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "col-md-12");
            __builder.AddAttribute(6, "b-fajghabgcf");
            __builder.OpenElement(7, "div");
            __builder.AddAttribute(8, "class", "form-group");
            __builder.AddAttribute(9, "b-fajghabgcf");
            __builder.OpenElement(10, "div");
            __builder.AddAttribute(11, "class", "putbox-content");
            __builder.AddAttribute(12, "b-fajghabgcf");
            __builder.AddMarkupContent(13, "<div class=\"content-title\" b-fajghabgcf>Categories Filter</div>\r\n          ");
            __builder.OpenElement(14, "div");
            __builder.AddAttribute(15, "class", "combobox");
            __builder.AddAttribute(16, "b-fajghabgcf");
#nullable restore
#line 18 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
          if (categories== null)  {  } else
           {
             

#line default
#line hidden
#nullable disable
            __builder.OpenElement(17, "select");
            __builder.AddAttribute(18, "class", "form-control");
            __builder.AddAttribute(19, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 21 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                                      OnFilterProducts

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(20, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 21 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                                                                 query.CategoryId

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(21, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => query.CategoryId = __value, query.CategoryId));
            __builder.SetUpdatesAttributeName("value");
            __builder.AddAttribute(22, "b-fajghabgcf");
            __builder.OpenElement(23, "option");
            __builder.AddAttribute(24, "value");
            __builder.AddAttribute(25, "b-fajghabgcf");
            __builder.AddContent(26, "All Products");
            __builder.CloseElement();
#nullable restore
#line 23 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                  foreach (var item in categories)
                  {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(27, "option");
            __builder.AddAttribute(28, "value", 
#nullable restore
#line 25 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                     item.Id

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(29, "b-fajghabgcf");
#nullable restore
#line 25 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
__builder.AddContent(30, item.Name);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
#nullable restore
#line 26 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                   }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
#nullable restore
#line 28 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
          }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n\r\n     ");
            __builder.OpenElement(32, "div");
            __builder.AddAttribute(33, "class", "col-md-24");
            __builder.AddAttribute(34, "b-fajghabgcf");
            __builder.OpenElement(35, "ol");
            __builder.AddAttribute(36, "class", "list-items");
            __builder.AddAttribute(37, "b-fajghabgcf");
#nullable restore
#line 46 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
   if (productSkus == null)
{

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(38, @"<li class=""list-items-row"" b-fajghabgcf><div class=""entity-list "" b-fajghabgcf><div class=""entity-list-item "" b-fajghabgcf><div class=""item-icon"" b-fajghabgcf><span class=""glyph glyph-warning"" b-fajghabgcf></span></div>
           <div class=""item-content-secondary"" b-fajghabgcf><div class=""content-text-tertiary "" b-fajghabgcf><span class=""label label-danger"" b-fajghabgcf>Nothing here</span></div></div>
           <div class=""content-text-tertiary "" b-fajghabgcf><div class=""progress-bar"" b-fajghabgcf><div class=""progress-circle"" b-fajghabgcf></div>
      <div class=""progress-circle"" b-fajghabgcf></div>
      <div class=""progress-circle"" b-fajghabgcf></div>
      <div class=""progress-circle"" b-fajghabgcf></div>
      <div class=""progress-circle"" b-fajghabgcf></div></div></div></div></div></li>");
#nullable restore
#line 69 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
} else
{

      
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 73 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
     foreach (var item in productSkus.Items)
    {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(39, "li");
            __builder.AddAttribute(40, "class", "list-items-row");
            __builder.AddAttribute(41, "b-fajghabgcf");
            __builder.OpenElement(42, "div");
            __builder.AddAttribute(43, "class", "row");
            __builder.AddAttribute(44, "b-fajghabgcf");
            __builder.OpenElement(45, "div");
            __builder.AddAttribute(46, "class", "entity-list ");
            __builder.AddAttribute(47, "b-fajghabgcf");
            __builder.OpenElement(48, "div");
            __builder.AddAttribute(49, "class", "entity-list-item ");
            __builder.AddAttribute(50, "b-fajghabgcf");
            __builder.AddMarkupContent(51, "<div class=\"item-icon\" b-fajghabgcf><span class=\"glyph glyph-chevron-right\" b-fajghabgcf></span></div>\r\n          ");
            __builder.OpenElement(52, "div");
            __builder.AddAttribute(53, "class", "item-content-secondary");
            __builder.AddAttribute(54, "b-fajghabgcf");
#nullable restore
#line 83 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
__builder.AddContent(55, item.SkuValue.SkuValueName);

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(56, "\r\n              ");
            __builder.OpenElement(57, "div");
            __builder.AddAttribute(58, "class", "content-text-tertiary ");
            __builder.AddAttribute(59, "b-fajghabgcf");
            __builder.OpenElement(60, "span");
            __builder.AddAttribute(61, "class", "label label-warning");
            __builder.AddAttribute(62, "b-fajghabgcf");
#nullable restore
#line 84 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
__builder.AddContent(63, item.SkuValue.OptionValue.Option.Product.ProductCategory.Name);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(64, "\r\n\r\n          ");
            __builder.OpenElement(65, "div");
            __builder.AddAttribute(66, "class", "item-content-primary");
            __builder.AddAttribute(67, "b-fajghabgcf");
            __builder.OpenElement(68, "div");
            __builder.AddAttribute(69, "class", "col-md-18");
            __builder.AddAttribute(70, "b-fajghabgcf");
            __builder.OpenElement(71, "div");
            __builder.AddAttribute(72, "class", "content-text-primary ");
            __builder.AddAttribute(73, "b-fajghabgcf");
#nullable restore
#line 91 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
__builder.AddContent(74, item.SkuValue.OptionValue.Option.Product.ProductName);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(75, "\r\n            ");
            __builder.OpenElement(76, "div");
            __builder.AddAttribute(77, "class", "content-text-secondary");
            __builder.AddAttribute(78, "b-fajghabgcf");
#nullable restore
#line 92 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
__builder.AddContent(79, item.SkuValue.OptionValue.OptionValueName);

#line default
#line hidden
#nullable disable
            __builder.AddContent(80, " - KES ");
#nullable restore
#line 92 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
__builder.AddContent(81, item.Price);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(82, "\r\n              ");
            __builder.OpenElement(83, "div");
            __builder.AddAttribute(84, "class", "col-md-6");
            __builder.AddAttribute(85, "b-fajghabgcf");
            __builder.OpenElement(86, "div");
            __builder.AddAttribute(87, "class", "content-text-tertiary ");
            __builder.AddAttribute(88, "b-fajghabgcf");
            __builder.OpenElement(89, "div");
            __builder.AddAttribute(90, "class", "checkbox");
            __builder.AddAttribute(91, "b-fajghabgcf");
            __builder.OpenElement(92, "label");
            __builder.AddAttribute(93, "b-fajghabgcf");
#nullable restore
#line 100 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                     if (@item.Id > 0)
                    {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(94, "input");
            __builder.AddAttribute(95, "name", "AreChecked");
            __builder.AddAttribute(96, "type", "checkbox");
            __builder.AddAttribute(97, "value", 
#nullable restore
#line 103 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                    item.Id

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(98, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 104 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                         eventArgs => { CheckChanged
                            (saveCompositeProductDTO, eventArgs.Value, item.Id, item.Price);
                            }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(99, "b-fajghabgcf");
            __builder.CloseElement();
#nullable restore
#line 108 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                    }
                     else
                     {
                         

#line default
#line hidden
#nullable disable
            __builder.OpenElement(100, "input");
            __builder.AddAttribute(101, "name", "AreChecked");
            __builder.AddAttribute(102, "type", "checkbox");
            __builder.AddAttribute(103, "value", 
#nullable restore
#line 113 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                    item.Id

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(104, "checked");
            __builder.AddAttribute(105, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 114 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                       eventArgs => { CheckChanged (saveCompositeProductDTO, eventArgs.Value, item.Id,item.Price);}

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(106, "b-fajghabgcf");
            __builder.CloseElement();
#nullable restore
#line 117 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                     }

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(107, "<span b-fajghabgcf>Pick</span>");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 135 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
    }

#line default
#line hidden
#nullable disable
            __builder.OpenElement(108, "li");
            __builder.AddAttribute(109, "b-fajghabgcf");
            __builder.OpenElement(110, "button");
            __builder.AddAttribute(111, "disabled", 
#nullable restore
#line 138 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                    IsDisabled

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(112, "class", "btn btn-secondary");
            __builder.AddAttribute(113, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 138 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                                                                    (() => ListUpdated())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(114, "b-fajghabgcf");
            __builder.AddContent(115, "Create Composite Selection");
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 140 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
   

}

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 166 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
       

    ProductSkuDTO productSkuValueDTO { get; set; } = new ProductSkuDTO();
    ProductSkuQueryDTO query { get; set; } = new ProductSkuQueryDTO();

    protected bool IsDisabled = true;
  
    string error { get; set; }
     [Inject]
    public IProductSkuDataService ProductSkuDataService { get; set; }
    [Inject]
    public ICompositeProductDataService CompositeProductDataService { get; set; }

      [Inject]
    public ICategoryDataService CategoryDataService { get; set; }


    

#line default
#line hidden
#nullable disable
#nullable restore
#line 183 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                

        // private ProductSkuDTO[] productSkus { get; set; }
        private QueryResultDTO<ProductSkuDTO> productSkus { get; set; }
        private ProductCategoryDTO[] categories { get; set; }

        [Parameter]
        public int Id { get; set; }

        SaveCompositeProductDTO saveCompositeProductDTO { get; set; } = new SaveCompositeProductDTO();
        string errorTwo { get; set; }
        protected override async Task OnInitializedAsync()
        {

            await   OnFilterProducts();
            categories = await CategoryDataService.GetCategories();




        }



        protected void CheckChanged(SaveCompositeProductDTO saveProduct,
                                     object checkValue, int id, decimal price)
        {

            int number = 0;
            if (id > number)
            {
                number = (int)id;
                // saveProduct = await CompositeProductDataService.CreateCompositeProduct(saveCompositeProductDTO);
                if((bool)checkValue)
                {
                  
                    saveCompositeProductDTO.CombinedProducts.Add(number);
                    saveCompositeProductDTO.CombinedProducts.ToList();
                    saveCompositeProductDTO.TotalPrice += price;
                    Console.WriteLine($"First total: {saveCompositeProductDTO.TotalPrice}");
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 223 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                     if (saveCompositeProductDTO.CombinedProducts.Count > 1)
                    {
                      IsDisabled = false;  
                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 226 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                     


                    //  await CompositeProductDataService.CreateCompositeProduct(saveProduct);
                    //  toastService.ShowToast($"Product Created Succesfuly", ToastLevel.Success);
                } else 
                {
                    saveCompositeProductDTO.TotalPrice = saveCompositeProductDTO.TotalPrice - price;
                    saveCompositeProductDTO.CombinedProducts.Remove(number);
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 235 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                     if (saveCompositeProductDTO.CombinedProducts.Count < 2)
                    {
                      IsDisabled = true;  
                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 238 "C:\Users\Hp\Desktop\dddEven\TwoTouch\BlazorWithIdentity\src\BlazorWithIdentity.Client\Pages\ProductList.razor"
                     
                    
                    Console.WriteLine($"Last total: {saveCompositeProductDTO.TotalPrice}");
                    
                    
                    
                }
            } else {
                    
            }

        }


        async Task ListUpdated()
        {
            await CompositeProductDataService.CreateCompositeProduct(saveCompositeProductDTO);
            toastService.ShowToast($"Products Created Succesfuly", ToastLevel.Success);
            saveCompositeProductDTO.CombinedProducts.Clear();

            saveCompositeProductDTO.TotalPrice = 0;
         
            navigationManager.NavigateTo("/composite");
        }
        private  async Task OnFilterProducts()
        {
              var props = GetProperties(query);
            foreach (var prop in props)
		{
                if (prop.Key != "CategoryId")
                {
                 // productSkus = await ProductSkuDataService.GetProductSkus("");  
                } else if (prop.Key == "CategoryId") {

                    var combined = $"{prop.Key}={prop.Value}";
                    productSkus = await ProductSkuDataService.GetProductSkus(combined);

                 //   await OnInitializedAsync();
                    // await OnFilterProducts();
                 
                 
                 } else if (prop.Key == "OptionValueId") {

                    var combined = $"{prop.Key}={prop.Value}";
                    productSkus = await ProductSkuDataService.GetProductSkus(combined);
                }
		}
        }

        private static Dictionary<string, string> GetProperties(object obj)
	{
		var props = new Dictionary<string, string>();
		if (obj == null)
			return props;

		var type = obj.GetType();
		foreach (var prop in type.GetProperties())
		{
			var val = prop.GetValue(obj, new object[] { });
			var valStr = val == null ? "" : val.ToString();
			props.Add(prop.Name, valStr);
		}

		return props;
	}


  

     

            

 

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ToastService toastService { get; set; }
    }
}
#pragma warning restore 1591
